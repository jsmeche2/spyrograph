# -*- coding: utf-8 -*-
"""
Created on Sun Jul  7 15:29:36 2019

@author: jsmec
"""

import numpy as np
import matplotlib.pyplot as plt
import spyrograph as spy
# =============================================================================
# 
# R_list = [.5,1,1.5]
# r_list = [.5,1,1.5]
# d_list = [.5,1,1.5]
# epi_list = []
# 
# theta = 6*np.pi
# step_size = 0.01
# 
# for R in R_list:
#     for r in r_list:
#         for d in d_list:
#             epi = spy.Epitrochoid(R,r,d,theta,step_size)
#             epi.plot("%s %s %s.png" % (R,r,d) )
#             epi_list.append(epi)
# 
# for epi in epi_list:
#     epi.plot()
# =============================================================================

"""
The following classes are tested:
    1. General Roulette - this class can't actually make any real roulettes, but is included to allow inheritane of common methods
    2. Epitrochoid - https://en.wikipedia.org/wiki/Epitrochoid
    3. Cycloidal Disk - An epitrochoid, but reformulated to take different parameters - found in a solidworks tutorial on how to make a cycloidal drive system
    4. Limacon - https://en.wikipedia.org/wiki/Lima%C3%A7on
    5. Epicycloid - https://en.wikipedia.org/wiki/Epicycloid
    6. Cardioid - https://en.wikipedia.org/wiki/Cardioid
    7. Hypotrochoid - https://en.wikipedia.org/wiki/Hypotrochoid
    8. Hypocycloid - https://en.wikipedia.org/wiki/Hypocycloid
    9. Hypocycloid_k - https://en.wikipedia.org/wiki/Hypocycloid
    10. Trochoid - https://en.wikipedia.org/wiki/Trochoid
    11. Cycloid - https://en.wikipedia.org/wiki/Cycloid
"""

#epitrochoid
epit = spy.Epitrochoid(3,1,.5)
#epit.plot("epit.png")
epit.plot_polar("epit_polar.png")

#cycloidal disk
cyc = spy.CycloidalDisk(10,1.5,.75, 10)
#cyc.plot("cyc.png")
cyc.plot_polar("cyc_polar.png")

#limacon
lim = spy.Limacon(3,4)
#lim.plot("lim.png")
lim.plot_polar("lim_polar.png")

#Epicycloid
epic = spy.Epicyloid(3,1)
#epic.plot("epic.png")
epic.plot_polar("epic_polar.png")

#cardioid
card = spy.Cardioid(3)
#card.plot("card.png")
card.plot_polar("card_polar.png")

#hypotrochoid
hypot = spy.Hypotrochoid(3,1,.5)
#hypot.plot("hypot.png")
hypot.plot_polar("hypot_polar.png")

#hypocycloid
hypoc = spy.Hypocycloid(3,2)
#hypoc.plot("hypoc.png")
hypoc.plot_polar("hypoc_polar.png")

#hypocycloid_k
hypok = spy.HypocycloidK(2,3)
#hypok.plot("hypok.png")
hypok.plot_polar("hypok_polar.png")

#trochoid
troch = spy.Trochoid(5,1)
#troch.plot("troch.png")
troch.plot_polar("troch_polar.png")

#Cycloid
cycl = spy.Cycloid(5)
#cycl.plot("cycl.png")
cycl.plot_polar("cycl_polar.png")


