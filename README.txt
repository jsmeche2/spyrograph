This module includes classes for creating roulette curves; those curves commonly
created by the spirograph toy. The curves included in this module include:
    1. General Roulette - this class can't actually make any real roulettes, but is included to allow inheritane of common methods
    2. Epitrochoid - https://en.wikipedia.org/wiki/Epitrochoid
    3. Cycloidal Disk - An epitrochoid, but reformulated to take different parameters - found in a solidworks tutorial on how to make a cycloidal drive system
    4. Limacon - https://en.wikipedia.org/wiki/Lima%C3%A7on
    5. Epicycloid - https://en.wikipedia.org/wiki/Epicycloid
    6. Cardioid - https://en.wikipedia.org/wiki/Cardioid
    7. Hypotrochoid - https://en.wikipedia.org/wiki/Hypotrochoid
    8. Hypocycloid - https://en.wikipedia.org/wiki/Hypocycloid
    9. Hypocycloid_k - https://en.wikipedia.org/wiki/Hypocycloid
    10. Trochoid - https://en.wikipedia.org/wiki/Trochoid
    11. Cycloid - https://en.wikipedia.org/wiki/Cycloid

Curves yet to be implemented:
    12. Cyclogon and variants