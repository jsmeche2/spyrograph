# -*- coding: utf-8 -*-
"""
MIT License
Copyright (c) 2019 Jonathan Schultz

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

"""
This module includes classes for creating roulette curves; those curves commonly
created by the spirograph toy. The curves included in this module include:
    1. General Roulette - this class can't actually make any real roulettes, but is included to allow inheritane of common methods
    2. Epitrochoid - https://en.wikipedia.org/wiki/Epitrochoid
    3. Cycloidal Disk - An epitrochoid, but reformulated to take different parameters - found in a solidworks tutorial on how to make a cycloidal drive system
    4. Limacon - https://en.wikipedia.org/wiki/Lima%C3%A7on
    5. Epicycloid - https://en.wikipedia.org/wiki/Epicycloid
    6. Cardioid - https://en.wikipedia.org/wiki/Cardioid
    7. Hypotrochoid - https://en.wikipedia.org/wiki/Hypotrochoid
    8. Hypocycloid - https://en.wikipedia.org/wiki/Hypocycloid
    9. Hypocycloid_k - https://en.wikipedia.org/wiki/Hypocycloid
    10. Trochoid - https://en.wikipedia.org/wiki/Trochoid
    11. Cycloid - https://en.wikipedia.org/wiki/Cycloid

Curves yet to be implemented:
    12. Cyclogon and variants
"""

#A python module that can make some shapes that can be made by the spirograph toy
import numpy as np
import matplotlib.pyplot as plt #change this to normal matplotlib as mtl

class Roulette:
    """
    Generic class for making roulette-type curves. Used as a generic class, which other classes will inherit from
    """
    def __init__(self):
        self.theta_array = np.empty((1,1))
        self.x_array = np.empty((1,1))
        self.y_array = np.empty((1,1))
        self.phi_array = np.empty((1,1))
        self.polar_r_array = np.empty((1,1))
    
    def plot(self,filename = None, plot_title = "Plot of the Curve"):
        #plot the parameters of the curve, like R, r, d, etc.
        #also plot the center of the circle, and the inner circle, eventually make this into an option
        fig, ax = plt.subplots()
        ax.plot(self.x_array,self.y_array)
        ax.set(xlabel="X Axis", ylabel = "Y Axis", title= plot_title)
        ax.set_aspect('equal', adjustable = 'box')
        
        if filename != None:
            fig.savefig(filename, transparent = False, dpi=500, quality = 95, bbox_inches = 'tight')
    
    def plot_polar(self,filename = None, plot_title = "Polar Plot of the Curve"):
        #plot the parameters of the curve, like R, r, d, on a polar plot
        #also plot the center of the circle, and the inner circle, eventually make this into an option
        fig = plt.figure()
        ax = fig.add_subplot(111, projection = 'polar')
        ax.plot(self.phi_array,self.polar_r_array)
        ax.set(title= plot_title)
        ax.set_aspect('equal', adjustable = 'box')
        
        if filename != None:
            fig.savefig(filename, transparent = False, dpi=500, quality = 95, bbox_inches = 'tight')
        

class Epitrochoid(Roulette):
    
    def __init__(self,R,r,d,theta=2*np.pi,step=.01):
        self.R = R #radius of main, stationary circle
        self.r = r #radius of outer, rolling circle
        self.d = d #distance from center of rolling circle that the point of drawing is located
        self.theta = theta #the maximum radians that will be traced out
        self.step = step #the step size used during discretization of the curve parametric equations
        self.delta = 2*self.d #the difference between inner envelope and outer envelope of the curve
        self.radial_ratio = self.R/self.r #ratio between main and outer circles radii, also, number of lobes is one full rotation about the main circle
        self.Ro = self.R + self.r + self.d #Radius of the outer lobes of the epitrochoid
        self.Ri = self.R + self.r - self.d #Radius of the inner lobes of the epitrochoid
        self.discretize()
    
    def discretize(self):
        #use the theta and step parameters to create a 3-column array of the discretized curve.
        #the first column is theta, the 2nd column is the x-coordinate, 3rd column is the y-coordinate
        #all information is stored in a 3-column numpy array.
        self.theta_array = np.linspace(0,self.theta,self.theta/self.step)
        self.x_array = (self.R+self.r)*np.cos(self.theta_array) - self.d*np.cos((self.R+self.r)/self.r*self.theta_array)
        self.y_array = (self.R+self.r)*np.sin(self.theta_array) - self.d*np.sin((self.R+self.r)/self.r*self.theta_array)
        self.phi_array = np.arctan2(self.y_array,self.x_array)
        self.polar_r_array = np.sqrt(self.x_array**2+self.y_array**2)


class CycloidalDisk(Roulette):
    """
    A cycloidal disk is used to create a cycloidal gear train.
    The equations found in this class were taken from a Solidworks tutorial that I found online
    This class does not appear to be doing what solidworks is producing
    R = Radius of the Rotor
    Rp = Radius of the pins
    E = Eccentricity (offset of the driving shaft)
    N = Number of pins (Not number of lobes on the disk)
    """
    def __init__(self,R,Rp,E,N,theta=2*np.pi,step=.01):
        self.R = R
        self.Rp = Rp
        self.E = E
        self.N = N
        self.theta = theta
        self.step = step
        self.discretize()
    
    def discretize(self):
        self.theta_array = np.linspace(0,self.theta,self.theta/self.step)
        psi_array = -np.arctan2((np.sin((1.0-self.N)*self.theta_array)),((self.R/self.E*self.N)-np.cos((1.0-self.N)*self.theta_array)))
        self.x_array = self.R*np.cos(self.theta_array)-self.Rp*np.cos(self.theta_array-psi_array)-self.E*np.cos(self.N*self.theta_array)
        self.y_array = -self.R*np.sin(self.theta_array)+self.Rp*np.sin(self.theta_array-psi_array)+self.E*np.sin(self.N*self.theta_array)
        self.phi_array = np.arctan2(self.y_array,self.x_array)
        self.polar_r_array = np.sqrt(self.x_array**2+self.y_array**2)

class Limacon(Epitrochoid):
    """
    A roulette formed by the path of a point fixed to a circle when that circle
    rolls outside of a circle of equal radius. It is an Epitrochoid where r = R
    """
    def __init__(self,R,d,theta=2*np.pi,step=.01):
        self.R = R #radius of main, stationary circle
        self.r = R #radius of outer, rolling circle
        self.d = d #distance from center of rolling circle that the point of drawing is located
        self.theta = theta #the maximum radians that will be traced out
        self.step = step #the step size used during discretization of the curve parametric equations
        self.delta = 2*self.d #the difference between inner envelope and outer envelope of the curve
        self.radial_ratio = self.R/self.r #ratio between main and outer circles radii, also, number of lobes is one full rotation about the main circle
        self.Ro = self.R + self.r + self.d #Radius of the outer lobes of the epitrochoid
        self.Ri = self.R + self.r - self.d #Radius of the inner lobes of the epitrochoid
        self.discretize()

class Epicyloid(Epitrochoid):
    """
    An Epitrochoid in which d=r
    """
    
    def __init__(self,R,r,theta=2*np.pi,step=.01):
        self.R = R #radius of main, stationary circle
        self.r = r #radius of outer, rolling circle
        self.d = r #distance from center of rolling circle that the point of drawing is located
        self.theta = theta #the maximum radians that will be traced out
        self.step = step #the step size used during discretization of the curve parametric equations
        self.delta = 2*self.d #the difference between inner envelope and outer envelope of the curve
        self.radial_ratio = self.R/self.r #ratio between main and outer circles radii, also, number of lobes is one full rotation about the main circle
        self.Ro = self.R + self.r + self.d #Radius of the outer lobes of the epitrochoid
        self.Ri = self.R + self.r - self.d #Radius of the inner lobes of the epitrochoid
        self.discretize()

class Cardioid(Epitrochoid):
    """
    The common Cardioid
    """
    def __init__(self,R,theta=2*np.pi,step=.01):
        self.R = R #radius of main, stationary circle
        self.r = R #radius of outer, rolling circle
        self.d = R #distance from center of rolling circle that the point of drawing is located
        self.theta = theta #the maximum radians that will be traced out
        self.step = step #the step size used during discretization of the curve parametric equations
        self.delta = 2*self.d #the difference between inner envelope and outer envelope of the curve
        self.radial_ratio = self.R/self.r #ratio between main and outer circles radii, also, number of lobes is one full rotation about the main circle
        self.Ro = self.R + self.r + self.d #Radius of the outer lobes of the epitrochoid
        self.Ri = self.R + self.r - self.d #Radius of the inner lobes of the epitrochoid
        self.discretize()

class Hypotrochoid(Epitrochoid):
    """
    Inherits from the Epitrochoid class, since the only thing that
    is different is the parametric equations for the two curves.
    """
    
    def discretize(self):
        #use the theta and step parameters to create a 3-column array of the discretized curve.
        #the first column is theta, the 2nd column is the x-coordinate, 3rd column is the y-coordinate
        #all information is stored in a 3-column numpy array.
        self.theta_array = np.linspace(0,self.theta,self.theta/self.step)
        self.x_array = (self.R-self.r)*np.cos(self.theta_array) + self.d*np.cos((self.R-self.r)/self.r*self.theta_array)
        self.y_array = (self.R-self.r)*np.sin(self.theta_array) - self.d*np.sin((self.R-self.r)/self.r*self.theta_array)
        self.phi_array = np.arctan2(self.y_array,self.x_array)
        self.polar_r_array = np.sqrt(self.x_array**2+self.y_array**2)


class Hypocycloid(Hypotrochoid):
    """
    A special case of the Hypotrochoid in which d=r. In this module, only the constructor function is different.
    The constructor sets d=r, then everything else from the Hypotrochoid method is inherited as-is.
    """
    def __init__(self,R,r,theta=2*np.pi,step=.01):
        self.R = R #radius of main, stationary circle
        self.r = r #radius of outer, rolling circle
        self.d = r #distance from center of rolling circle that the point of drawing is located
        self.theta = theta #the maximum radians that will be traced out
        self.step = step #the step size used during discretization of the curve parametric equations
        self.delta = 2*self.d #the difference between inner envelope and outer envelope of the curve
        self.radial_ratio = self.R/self.r #ratio between main and outer circles radii, also, number of lobes is one full rotation about the main circle
        self.discretize()

class HypocycloidK(Roulette):
    """
    A special case of the Hypocycloid in which d=r.
    This version of the function uses the k parameterization
    The __init__, radial_ratio, and discretize functions are the functions that are overridden.
    """
    def __init__(self,k,r,theta=2*np.pi,step=.01):
        self.k = k
        self.r = r
        self.theta = theta
        self.step = step
        self.radial_ratio = False
        self.discretize()
    
    def discretize(self):
        #use the theta and step parameters to create a 3-column array of the discretized curve.
        #the first column is theta, the 2nd column is the x-coordinate, 3rd column is the y-coordinate
        #all information is stored in a 3-column numpy array.
        self.theta_array = np.linspace(0,self.theta,self.theta/self.step)
        self.x_array = self.r*(self.k-1)*np.cos(self.theta_array) + self.r*np.cos((self.k-1)*self.theta_array)
        self.y_array = self.r*(self.k-1)*np.sin(self.theta_array) - self.r*np.sin((self.k-1)*self.theta_array)
        self.phi_array = np.arctan2(self.y_array,self.x_array)
        self.polar_r_array = np.sqrt(self.x_array**2+self.y_array**2)
    
class Trochoid(Roulette):
    """
    The generalized Trochoid, of which the Common Cycloid, Curtate Cycloid, and Prolate Cycloid are members 
    """
    def __init__(self, r, d, theta=2*np.pi, step=.01):
        self.r = r
        self.d = d
        self.theta = theta
        self.step = step
        self.discretize()
    
    def discretize(self):
        self.theta_array = np.linspace(0,self.theta,self.theta/self.step)
        self.x_array = self.r*self.theta_array - self.d*np.sin(self.theta_array)
        self.y_array = self.r - self.d*np.cos(self.theta_array)
        self.phi_array = np.arctan2(self.y_array,self.x_array)
        self.polar_r_array = np.sqrt(self.x_array**2+self.y_array**2)

class Cycloid(Trochoid):
    """
    The common cycloid
    """
    def __init__(self, r, theta=2*np.pi, step=.01):
        self.r = r
        self.d = r
        self.theta = theta
        self.step = step
        self.discretize()